use clap::{App, Arg};
use rayon::prelude::*;
use std::io;
use std::io::Write;
use std::process::{Command, Output, Stdio};
use std::fs;

fn is_up(machine: &str) -> bool {
    Command::new("ping")
        .stderr(Stdio::null())
        .stdout(Stdio::null())
        .arg("-c 1")
        .arg("-w 1")
        .arg(machine)
        .status()
        .expect("failed to execute ping")
        .success()
}

fn run_on(cmd: String, machine: &String) -> Output {
    Command::new("ssh")
        .arg(machine)
        .arg(cmd)
        .output()
        .expect("failed to execute ping")
}

fn stop_reading(res: io::Result<usize>) -> bool {
    match res {
        Ok(n) if n == 0 => true,
        Err(_e) => true,
        _ => false,
    }
}

fn main() {
    // Get the user name as a default when none is provided
    let mut r = Command::new("whoami")
        .output()
        .expect("failed to execute whoami")
        .stdout;
    r.truncate(r.len() - 1);
    let un = std::str::from_utf8(&r).unwrap();

    let matches = App::new("balancer")
        .version(env!("CARGO_PKG_VERSION"))
        .author(env!("CARGO_PKG_AUTHORS"))
        .about("Remote batch execution load balancer")
        .after_help("Reads commands from stdin and executes each on one available machine. It connects to the machines via ssh and the given username. By default it checks if the machines are reachable on statup (via ping).")
        .arg(
            Arg::with_name("machines")
                .short("m")
                .long("machines")
                .value_name("FILE")
                .help("A file with all the hosts that should be used")
                .takes_value(true)
                .required(true),
        )
        .arg(
            Arg::with_name("silence")
                .short("s")
                .long("silence")
                .takes_value(false)
                .help("Supresses output"),
        )
        .arg(
            Arg::with_name("online")
                .short("o")
                .long("online")
                .takes_value(false)
                .help("If you know that all machines in your list are online set this flag to skip the costly check"),
        )
        .arg(
            Arg::with_name("user")
                .short("u")
                .long("user")
                .takes_value(true)
                .default_value(un)
                .help("The user to use for the ssh connections")
        )
        .get_matches();

    let no_print = matches.is_present("silence");
    let user = matches.value_of("user").unwrap();
    let skip_online = matches.is_present("online");

    let all_machines = fs::read_to_string(matches.value_of("machines").unwrap()).expect("Couldn't read machine file"); // separateor for lifetime reasons
    let machines: Vec<&str> = all_machines.par_lines().filter(|x| skip_online || is_up(x)).collect();

    // Create a thread pool with a thread per available machine
    let pool = rayon::ThreadPoolBuilder::new()
        .num_threads(machines.len())
        .build()
        .unwrap();
    // Worker function for each thread
    let tf = |cmd: String| {
        || {
            if let Some(idx) = &pool.current_thread_index() {
                // Given that we have a thread per available machine we deterine the designated machine via the thread id
                let target = &machines[*idx];
                // Login via the given user
                let url = format!("{}@{}", user, target);
                if ! no_print {
                    println!("{} running: {}:", url, cmd);
                }
                let res = run_on(cmd, &url);
                if ! no_print {
                    println!("Output:");
                    io::stdout().write_all(&res.stdout).unwrap();
                    println!("Errors:");
                    io::stderr().write_all(&res.stderr).unwrap();
                    println!("\n")
                }
            } else {
                println!("Couldn't get the thread index");
            }
        }
    };
    // Read commands from stdin until error or EOF
    loop {
        let mut cmd = String::new();
        if stop_reading(io::stdin().read_line(&mut cmd)) {
            break;
        }
        pool.install(tf(cmd))
    }
}
